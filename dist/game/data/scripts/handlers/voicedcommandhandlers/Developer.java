/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.Config;
import com.l2jserver.extensions.wheelofwar.Factory;
import com.l2jserver.extensions.wheelofwar.domain.Point;
import com.l2jserver.extensions.wheelofwar.domain.Side;
import com.l2jserver.extensions.wheelofwar.utils.SpawnUtils;
import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.datatables.SpawnTable;
import com.l2jserver.gameserver.enums.AIType;
import com.l2jserver.gameserver.eternaltvt.TeamType;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2NpcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;
import com.l2jserver.gameserver.model.items.type.WeaponType;
import com.l2jserver.gameserver.model.stats.MoveType;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.util.Broadcast;
import com.l2jserver.util.Rnd;
import javaslang.Tuple2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Developer implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"dev1",
		"dev2",
		"spawnAll"
	};

	@Override
	public boolean useVoicedCommand(String command, L2PcInstance activeChar, String params)
	{
		if (command.contains("dev1"))
			dev1(activeChar);
		else if (command.contains("dev2"))
			dev2(activeChar);
		else if(command.contains("spawnAll"))
			ThreadPoolManager.getInstance().executeGeneral(() -> spawnAll(activeChar));
		
		return true;
	}

	public void dev3(L2PcInstance activeChar)
	{
		((L2Character)activeChar.getTarget()).getPlayerInfo().setSide(Side.ATTACKER);
		//Broadcast.toAllOnlinePlayers("hi");
		//SpawnUtils.spawnMonster(20006, 0,1,activeChar.getX(), activeChar.getY(), activeChar.getZ(), Side.ATTACKER);
	}

	private Predicate<L2NpcTemplate> getArcherTemplates()
	{
		return (template) -> {
			boolean conditionWeapon = template.getBaseAttackType() == WeaponType.BOW ||
				template.getBaseAttackType() == WeaponType.CROSSBOW;
			boolean conditionRange = template.getBaseAttackRange() > 500;
			boolean conditionSpeed = template.getBaseMoveSpeed(MoveType.RUN) > 10;
			return conditionWeapon && conditionRange && conditionSpeed;
		};
	}

	private List<L2NpcTemplate> filter(List<L2NpcTemplate> templates, Predicate<L2NpcTemplate> predicate)
	{
		return javaslang.collection.List.<L2NpcTemplate>empty()
				.appendAll(templates)
				.filter(predicate)
				.distinctBy((i,j) -> i.getCollisionHeight() == j.getCollisionHeight()
						&& i.getCollisionRadius() == j.getCollisionRadius() ? -1: 1)
				.toJavaList();
	}

	private void spawnAll(L2PcInstance actor)
	{
		int x = actor.getX();
		int y = actor.getY();
		List<L2NpcTemplate> templates = NpcData.getInstance().getAllMonstersOfLevel(IntStream.range(1,86).toArray());
		templates = filter(templates,getArcherTemplates());
		List<Tuple2<Integer,L2NpcTemplate>> data = javaslang.collection.List.range(1, templates.size())
				.map(i -> i*100)
				.zip(templates).toJavaList();

				for(Tuple2<Integer,L2NpcTemplate> t : data)
				{
				int spawnX = x + Rnd.get(1000);
				int spawnY = y + Rnd.get(1000);
				int spawnZ = actor.getZ() + 400;
				System.out.println(spawnX + " " + spawnY  + " " + spawnZ);
				this.spawnMonster(actor, String.valueOf(t._2.getId()),300,1,false,spawnX, spawnY, spawnZ);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				};
	}

	private void dev1(L2PcInstance actor)
	{
		int id = SpawnUtils.getRandomDragonTemplate().getId();
		SpawnUtils.spawnMonster(SpawnUtils.getRandomDragonTemplate(),1000013,0,1,actor.getX(), actor.getY(), actor.getZ(), Side.NEUTRAL,false);
		/**
		actor.sendMessage(actor.getX() + " " + actor.getY() + " " + actor.getZ());
		Point p = new Point(actor.getX(), actor.getY(), actor.getZ());
		Factory.getInstance().getPointsRepositoy().insertNewPoint(p, false).onFailure(t -> t.printStackTrace());

		 */
	}

	private void dev2(L2PcInstance actor)
	{
		actor.sendMessage(actor.getX() + " " + actor.getY() + " " + actor.getZ());
		Point p = new Point(actor.getX(), actor.getY(), actor.getZ());
		Factory.getInstance().getPointsRepositoy().insertNewPoint(p, true).onFailure(t -> t.printStackTrace());
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}

	public static class Base
	{
		private TeamType teamType;

		public Base(TeamType type)
		{
			teamType = type;
		}

		public void spawnWave()
        {

        }

        public void deleteWave()
        {

        }

        public BasePoint getBasePoint()
        {
            return null;
        }

        public void initBase(BasePoint point)
        {

        }
	}

	public static class BaseManager
    {
        Base attackers = new Base(TeamType.ATTACKERS);
        Base defenders = new Base(TeamType.DEFENDERS);

        public void onLogin(L2PcInstance player)
		{

		}

		public void onLogout(L2PcInstance player)
		{

		}

		public void setPlayerToTeam(TeamType type)
		{

		}
    }

    public static class BasePoint
    {

    }

    public static class PathPoint
    {

    }

    public static class PointManager
	{
		public BasePoint peek()
		{
			return null;
		}

		public BasePoint next()
		{
			return null;
		}
	}

	private void spawnMonster(L2PcInstance activeChar, String monsterId, int respawnTime, int mobCount, boolean permanent, int x, int y, int z)
	{
		L2Object target = activeChar.getTarget();
		if (target == null)
		{
			target = activeChar;
		}

		L2NpcTemplate template;
		if (monsterId.matches("[0-9]*"))
		{
			// First parameter was an ID number
			template = NpcData.getInstance().getTemplate(Integer.parseInt(monsterId));
		}
		else
		{
			// First parameter wasn't just numbers so go by name not ID
			template = NpcData.getInstance().getTemplateByName(monsterId.replace('_', ' '));
		}

		try
		{
			final L2Spawn spawn = new L2Spawn(template);
			if (Config.SAVE_GMSPAWN_ON_CUSTOM)
			{
				spawn.setCustom(true);
			}
			spawn.setX(x);
			spawn.setY(y);
			spawn.setZ(z);
			spawn.setAmount(mobCount);
			spawn.setHeading(activeChar.getHeading());
			spawn.setRespawnDelay(respawnTime);
			if (activeChar.getInstanceId() > 0)
			{
				spawn.setInstanceId(activeChar.getInstanceId());
				permanent = false;
			}
			else
			{
				spawn.setInstanceId(0);
			}
			// TODO add checks for GrandBossSpawnManager
			if (RaidBossSpawnManager.getInstance().isDefined(spawn.getId()))
			{
				activeChar.sendMessage("You cannot spawn another instance of " + template.getName() + ".");
			}
			else
			{
				if (template.isType("L2RaidBoss"))
				{
					spawn.setRespawnMinDelay(43200);
					spawn.setRespawnMaxDelay(129600);
					RaidBossSpawnManager.getInstance().addNewSpawn(spawn, 0, template.getBaseHpMax(), template.getBaseMpMax(), permanent);
				}
				else
				{
					SpawnTable.getInstance().addNewSpawn(spawn, permanent);
					spawn.init();
				}
				if (!permanent)
				{
					spawn.stopRespawn();
				}
				activeChar.sendMessage("Created " + template.getName() + " on " + target.getObjectId());
			}
		}
		catch (Exception e)
		{
			activeChar.sendPacket(SystemMessageId.TARGET_CANT_FOUND);
		}
	}
}
