/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.extensions.beeengine.BeeEngineManager;
import com.l2jserver.extensions.dressme.VisualArmorService;
import com.l2jserver.extensions.wheelofwar.Factory;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import javaslang.Tuple;
import javaslang.Tuple2;
import javaslang.Tuple3;
import javaslang.control.Try;

public class BeeEngine implements IVoicedCommandHandler {

    private BeeEngineManager manager = Factory.getInstance().getBeeEngineManager();

    private static final String[] VOICED_COMMANDS =
            {
                    "beeplayerset",
                    "beesummonset",
                    "beeskillset",
                    "beeplayerget",
                    "beesummonget",
                    "beeskillget"
            };

    @Override
    public boolean useVoicedCommand(String command, L2PcInstance activeChar, String params) {
        if (command.contains("beeplayerset"))
            getSetterArgs(params)
                    .onSuccess( result -> manager.saveByPlayerValue(result._1, result._2, result._3))
                    .onFailure(t -> activeChar.sendMessage(t.getMessage()));
        else if (command.contains("beesummonset"))
            getSetterArgs(params)
                    .onSuccess( result -> manager.saveBySummonValue(result._1, result._2, result._3))
                    .onFailure(t -> activeChar.sendMessage(t.getMessage()));
        else if (command.contains("beeskillset"))
            getSetterArgs(params)
                    .onSuccess( result -> manager.saveBySkillValue(result._1, result._2, result._3))
                    .onFailure(t -> activeChar.sendMessage(t.getMessage()));
        else if (command.contains("beeplayerget"))
            getGetterArgs(params)
                    .onSuccess(result -> {
                        float value = manager.getByClassAttribute(result._1, result._2);
                        activeChar.sendMessage("Value: "+ value);
                    })
                    .onFailure((t -> activeChar.sendMessage(t.getMessage())));
        else if (command.contains("beesummonget"))
            getGetterArgs(params)
                    .onSuccess(result -> {
                        float value = manager.getBySummonClass(result._1, result._2);
                        activeChar.sendMessage("Value: "+ value);
                    });
        else if (command.contains("beeskillget"))
            getGetterArgs(params)
                    .onSuccess(result -> {
                        float value = manager.getBySkillAttribute(result._1, result._2);
                        activeChar.sendMessage("Value: "+ value);
                    })
                    .onFailure(t -> activeChar.sendMessage(t.getMessage()));

        return true;
    }

    @Override
    public String[] getVoicedCommandList() {
        return VOICED_COMMANDS;
    }

    public Try<Tuple3<Integer,String,Float>> getSetterArgs(String command)
    {
        return Try.of(() -> {
            String[] parts = command.split(" ");
            int id = Integer.parseInt(parts[0]);
            String attribute = parts[1];
            float value = Float.parseFloat(parts[2]);
            return Tuple.of(id,attribute,value);
        });
    }

    public  Try<Tuple2<Integer,String>> getGetterArgs(String command)
    {
        return Try.of(() ->{
            String[] parts = command.split(" ");
            int id = Integer.parseInt(parts[0]);
            String attribute = parts[1];
            return Tuple.of(id,attribute);
        });
    }
}
